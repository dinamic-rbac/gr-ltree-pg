package gr_ltree_pg

import (
	"bitbucket.org/dinamic-rbac/gr-rbac"
	"context"
	"github.com/pkg/errors"
)

// Сервис получения дочерних групп с использованием расширения LTree для PostgreSQL
type lTreeGroupGetter struct {
	queryBuilder queryBuilderInterface
	client       sqlClientInterface
}

// Конструктор сервиса
func newLTreeGroupGetter(
	queryBuilder queryBuilderInterface,
	client sqlClientInterface,
) gr_rbac.GroupGetterInterface {
	return &lTreeGroupGetter{
		queryBuilder: queryBuilder,
		client:       client,
	}
}

// Получение вложенных групп по переданным ID родителей.
//
// По сути в данной библиотеке учитывается, что группы могут иметь вложенную (древовидную) структуру,
// поэтому для доступных напрямую пользовательских групп необходимо так же получить вложенные подгруппы.
// Данный метод как раз таки возвращает список подгрупп, доступных пользователю.
func (l lTreeGroupGetter) GetNestedGroupsByIds(ctx context.Context, groupId []string) (nestedGroups []string, err error) {
	query, variables := l.queryBuilder.BuildQuery(ctx, groupId)
	nestedGroups, err = l.client.GetRows(ctx, query, variables)

	if nil != err {
		err = errors.Wrap(err, `failed to load nested groups`)
	}

	return
}
