package gr_ltree_pg

import (
	"context"
	"reflect"
	"testing"
)

func Test_lTreeGroupGetter_GetNestedGroupsByIds(t *testing.T) {
	type fields struct {
		queryBuilder queryBuilderInterface
		client       sqlClientInterface
	}
	type args struct {
		ctx     context.Context
		groupId []string
	}
	tests := []struct {
		name             string
		fields           fields
		args             args
		wantNestedGroups []string
		wantErr          bool
	}{
		{
			name: "Если при выполнении запроса произошла ошибка, то возвращаем ошибку",
			fields: fields{
				queryBuilder: &queryBuilderMock{},
				client: &sqlClientMock{
					Result:  nil,
					IsError: true,
				},
			},
			args:             args{},
			wantNestedGroups: nil,
			wantErr:          true,
		},
		{
			name: "Если при выполнении запроса произошла ошибка, то возвращаем ошибку",
			fields: fields{
				queryBuilder: &queryBuilderMock{},
				client: &sqlClientMock{
					Result:  []string{"1", "2"},
					IsError: false,
				},
			},
			args:             args{},
			wantNestedGroups: []string{"1", "2"},
			wantErr:          false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := lTreeGroupGetter{
				queryBuilder: tt.fields.queryBuilder,
				client:       tt.fields.client,
			}
			gotNestedGroups, err := l.GetNestedGroupsByIds(tt.args.ctx, tt.args.groupId)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetNestedGroupsByIds() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotNestedGroups, tt.wantNestedGroups) {
				t.Errorf("GetNestedGroupsByIds() gotNestedGroups = %v, want %v", gotNestedGroups, tt.wantNestedGroups)
			}
		})
	}
}
