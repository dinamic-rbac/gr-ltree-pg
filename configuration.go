package gr_ltree_pg

// Конфигурация для построения запросов выборки
type Configuration struct {
	Table      string // Название таблицы для поиска
	Identifier string // Идентификатор (первичный ключ) в таблице
	Path       string // Поле пути LTree в таблице
}
