create table realm_structure_elements
(
    id     serial  not null
        constraint realm_structure_elements_pk
            primary key,
    type   integer not null,
    name   text    not null,
    parent integer default 1
        constraint realm_structure_elements_parent_fk
            references realm_structure_elements
            on update cascade on delete cascade,
    path   ltree   default ''::ltree,
    active boolean not null
);

alter table realm_structure_elements
    owner to postgres;

create index realm_structure_elements_parent_index
    on realm_structure_elements (parent);

create index realm_structure_element_path_idx
    on realm_structure_elements (path);

INSERT INTO public.realm_structure_elements (id, type, name, parent, path, active) VALUES (1, 1, '<Realm>', null, '1', true);
INSERT INTO public.realm_structure_elements (id, type, name, parent, path, active) VALUES (2, 2, 'Test', 1, '1.2', true);
INSERT INTO public.realm_structure_elements (id, type, name, parent, path, active) VALUES (3, 2, 'Test 2', 1, '1.3', true);
INSERT INTO public.realm_structure_elements (id, type, name, parent, path, active) VALUES (4, 2, 'Test 1.1', 2, '1.2.5', true);