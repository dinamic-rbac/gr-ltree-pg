package pgsql

import (
	"database/sql"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"
	"log"
)

var Connection *sql.DB

func init() {
	conf, err := pgx.ParseConfig("host=localhost user=postgres password=postgres dbname=test sslmode=disable")
	if nil != err {
		log.Fatalln(err.Error())
	}

	conf.PreferSimpleProtocol = true

	Connection, err = sql.Open("pgx", stdlib.RegisterConnConfig(conf))
	if err != nil {
		log.Fatalln(err)
	}

	Connection.SetMaxIdleConns(10)
	Connection.SetMaxOpenConns(20)
}
