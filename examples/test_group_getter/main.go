package main

import (
	"bitbucket.org/dinamic-rbac/gr-ltree-pg"
	"bitbucket.org/dinamic-rbac/gr-ltree-pg/examples/pgsql"
	"context"
	"log"
)

// Пример создания экземпляра геттера вложенных групп.
//
// Для работы примера необходимо запустить сборку Docker-Compose и залить в БД, находящейся
// в ней, файл dump.sql. В данном файле находится дамп тестовых данных.
func main() {
	getter, err := gr_ltree_pg.NewLTreeGroupGetter(pgsql.Connection, gr_ltree_pg.Configuration{
		Table:      "realm_structure_elements",
		Identifier: "id",
		Path:       "path",
	})

	if nil != err {
		log.Fatal(err)
	}

	permissions, err := getter.GetNestedGroupsByIds(context.Background(), []string{"2", "3"})
	if nil != err {
		log.Fatal(err)
	}

	log.Println(permissions)
}
