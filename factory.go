package gr_ltree_pg

import (
	"bitbucket.org/dinamic-rbac/gr-rbac"
	"database/sql"
	"github.com/pkg/errors"
)

// Фабрика сервиса получения дочерних групп
func NewLTreeGroupGetter(db *sql.DB, conf Configuration) (gr_rbac.GroupGetterInterface, error) {
	queryBuilder := newQueryBuilder(conf, newRequiredFieldChecker())

	err := queryBuilder.IsConfigurationValid()
	if nil != err {
		return nil, errors.Wrap(err, `configuration validation failed`)
	}

	return newLTreeGroupGetter(queryBuilder, newSqlClient(db)), nil
}
