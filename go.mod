module bitbucket.org/dinamic-rbac/gr-ltree-pg

go 1.14

require (
	bitbucket.org/dinamic-rbac/gr-rbac v1.0.0
	github.com/jackc/pgx/v4 v4.13.0
	github.com/jmoiron/sqlx v1.3.4 // indirect
	github.com/lib/pq v1.10.3
	github.com/pkg/errors v0.9.1
)
