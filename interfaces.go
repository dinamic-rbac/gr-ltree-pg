package gr_ltree_pg

import "context"

// Интерфейс клиента для выполнения запроса
type sqlClientInterface interface {
	// Получение строк с данными разрешений
	GetRows(ctx context.Context, query string, variables []interface{}) ([]string, error)
}

// Генератор запроса на получение дочерних групп
type queryBuilderInterface interface {
	// Тестирование конфигурации
	// Если конфигурация не валидна, возвращается ошибка
	IsConfigurationValid() error

	// Генерация запроса на получение дочерних групп
	// для переданного списка родительских групп
	BuildQuery(ctx context.Context, groupId []string) (query string, variables []interface{})
}

// Сервис проверки полей конфигурации
type requiredFieldCheckerInterface interface {
	// Проверка наличия полей в конфигурации
	//
	// Проверяет заполненость строковых полей в конфигурации.
	// Если какое-то из полей не заполнено, возвращает ошибку.
	IsFieldsAvailable(conf *Configuration, fields []string) error
}
