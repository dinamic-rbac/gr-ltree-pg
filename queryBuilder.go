package gr_ltree_pg

import (
	"context"
	"github.com/lib/pq"
)

// Генератор запроса на получение дочерних групп
type queryBuilder struct {
	conf    Configuration
	checker requiredFieldCheckerInterface
}

// Конструктор запроса
func newQueryBuilder(
	conf Configuration,
	checker requiredFieldCheckerInterface,
) queryBuilderInterface {
	return &queryBuilder{
		conf:    conf,
		checker: checker,
	}
}

// Тестирование конфигурации
// Если конфигурация не валидна, возвращается ошибка
func (q queryBuilder) IsConfigurationValid() error {
	return q.checker.IsFieldsAvailable(&q.conf, []string{
		`Table`,
		`Identifier`,
		`Path`,
	})
}

// Генерация запроса на получение дочерних групп
// для переданного списка родительских групп
func (q queryBuilder) BuildQuery(ctx context.Context, groupId []string) (query string, variables []interface{}) {
	query = `select sc.` + q.conf.Identifier + `::text from ` + q.conf.Table + ` pr left join ` + q.conf.Table + ` sc on sc.` + q.conf.Path + ` <@ pr.` + q.conf.Path + ` where pr.` + q.conf.Identifier + ` = any($1)`
	variables = []interface{}{pq.Array(groupId)}

	return
}
