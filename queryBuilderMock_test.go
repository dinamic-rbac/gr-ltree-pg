package gr_ltree_pg

import "context"

// Подставка для тестирования
type queryBuilderMock struct{}

// Тестирование конфигурации
// Если конфигурация не валидна, возвращается ошибка
func (q queryBuilderMock) IsConfigurationValid() error {
	return nil
}

// Генерация запроса на получение дочерних групп
// для переданного списка родительских групп
func (q queryBuilderMock) BuildQuery(ctx context.Context, groupId []string) (query string, variables []interface{}) {
	return "test", nil
}
