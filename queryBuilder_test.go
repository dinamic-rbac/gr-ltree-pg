package gr_ltree_pg

import (
	"context"
	"github.com/lib/pq"
	"reflect"
	"testing"
)

func Test_queryBuilder_BuildQuery(t *testing.T) {
	type fields struct {
		conf    Configuration
		checker requiredFieldCheckerInterface
	}
	type args struct {
		ctx     context.Context
		groupId []string
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantQuery     string
		wantVariables []interface{}
	}{
		{
			name: "тестирование генерации запроса",
			fields: fields{
				conf: Configuration{
					Table:      "realm_structure_elements",
					Identifier: "id",
					Path:       "path",
				},
				checker: nil,
			},
			args: args{
				ctx:     nil,
				groupId: []string{"1"},
			},
			wantQuery:     "select sc.id::text from realm_structure_elements pr left join realm_structure_elements sc on sc.path <@ pr.path where pr.id = any($1)",
			wantVariables: []interface{}{pq.Array([]string{"1"})},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := queryBuilder{
				conf:    tt.fields.conf,
				checker: tt.fields.checker,
			}
			gotQuery, gotVariables := q.BuildQuery(tt.args.ctx, tt.args.groupId)
			if gotQuery != tt.wantQuery {
				t.Errorf("BuildQuery() gotQuery = %v, want %v", gotQuery, tt.wantQuery)
			}
			if !reflect.DeepEqual(gotVariables, tt.wantVariables) {
				t.Errorf("BuildQuery() gotVariables = %v, want %v", gotVariables, tt.wantVariables)
			}
		})
	}
}
