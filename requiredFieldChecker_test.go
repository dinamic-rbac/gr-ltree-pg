package gr_ltree_pg

import "testing"

func Test_requiredFieldChecker_IsFieldsAvailable(t *testing.T) {
	type args struct {
		conf   *Configuration
		fields []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Передано не известное поле",
			args: args{
				conf:   &Configuration{},
				fields: []string{"Test"},
			},
			wantErr: true,
		},
		{
			name: "Передано поле без значения",
			args: args{
				conf:   &Configuration{Table: ""},
				fields: []string{"Table"},
			},
			wantErr: true,
		},
		{
			name: "Передан пустой список полей",
			args: args{
				conf:   &Configuration{Table: "test"},
				fields: []string{},
			},
			wantErr: false,
		},
		{
			name: "Передано 2 поля, поле без значения и со значением",
			args: args{
				conf:   &Configuration{Table: "", Identifier: "Test"},
				fields: []string{"Table", "Identifier"},
			},
			wantErr: true,
		},
		{
			name: "Передано 2 поля со значением",
			args: args{
				conf:   &Configuration{Table: "Test", Identifier: "Test"},
				fields: []string{"Table", "Identifier"},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := requiredFieldChecker{}
			if err := r.IsFieldsAvailable(tt.args.conf, tt.args.fields); (err != nil) != tt.wantErr {
				t.Errorf("IsFieldsAvailable() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
