package gr_ltree_pg

import (
	"context"
	"database/sql"
	"github.com/pkg/errors"
)

// Интерфейс клиента для выполнения запроса
type sqlClient struct {
	client *sql.DB
}

// Конструктор клиента
func newSqlClient(client *sql.DB) sqlClientInterface {
	return &sqlClient{
		client: client,
	}
}

// Получение строк с данными разрешений
func (s sqlClient) GetRows(ctx context.Context, query string, variables []interface{}) ([]string, error) {
	rows, err := s.client.QueryContext(ctx, query, variables...)
	if nil != err {
		return nil, errors.Wrap(err, `failed to query groups`)
	}

	stepSize := 1
	result := make([]string, 0, stepSize)

	for rows.Next() {
		if cap(result) == len(result) {
			newResult := make([]string, 0, cap(result)*2)

			for _, data := range result {
				newResult = append(newResult, data)
			}

			result = newResult
		}

		var id string
		err = rows.Scan(&id)
		if nil != err {
			return nil, errors.Wrap(err, `failed to parse result`)
		}

		result = append(result, id)
	}

	_ = rows.Close()

	return result, nil
}
