package gr_ltree_pg

import (
	"context"
	"errors"
)

// Подставка для тестирования
type sqlClientMock struct {
	Result  []string
	IsError bool
}

// Получение строк с данными разрешений
func (s sqlClientMock) GetRows(ctx context.Context, query string, variables []interface{}) ([]string, error) {
	if s.IsError {
		return nil, errors.New(`test err`)
	}

	return s.Result, nil
}
